'use strict'

import React, { Component } from 'react';
import { AppRegistry, Image, ListView, TouchableHighlight, StyleSheet,
  RecyclerViewBackedScrollView, Text, View, TouchableOpacity } from 'react-native'

import ViewContainer from './app/components/ViewContainer'
import _ from 'lodash'
import Icon from 'react-native-vector-icons/FontAwesome'

class takeabreak extends Component {

  constructor(props) {
    super(props)
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(this._getPeople({}))
    }
  }

  render() {
    return (
      <ViewContainer>
        <ListView
          style={{marginTop: 100}}
          initialListSize={10}
          dataSource={this.state.dataSource}
          renderScrollComponent={props => <RecyclerViewBackedScrollView {...props} />}
          renderSeparator={this._renderSeperator}
          renderRow={this._renderRow} />
      </ViewContainer>
    )
  }

  _renderRow(person) {
    return (
      <TouchableOpacity style={styles.personRow} onPress={(event) => console.log(person) }>
        <Text style={styles.personName}>{`${_.capitalize(person.firstName)} ${_.capitalize(person.lastName)}`}</Text>
        <View style={{flex:1}} />
        <Icon name="chevron-right" size={10} style={styles.personMoreIcon} />
      </TouchableOpacity>
    )
  }

  _renderSeperator(sectionID: number, rowID: number, adjacentRowHighlighted: bool) {
   return (
     <View
       key={`${sectionID}-${rowID}`}
       style={{
         height: adjacentRowHighlighted ? 4 : 1,
         backgroundColor: adjacentRowHighlighted ? '#3B5998' : '#CCCCCC',
       }}
     />
   )
 }

  _getPeople() {
    var people = [
      {firstName: "claudio", lastName: "santos"},
      {firstName: "rodrigo", lastName: "souza"},
      {firstName: "felipe", lastName: "fisher"},
      {firstName: "luis", lastName: "rico"},
      {firstName: "emerson", lastName: "alma"}
    ];

    return people;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },

  personRow: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    height: 50
  },

  personName: {
    marginLeft: 25
  },

  personMoreIcon: {
    color: "green",
    height: 10,
    width: 10,
    marginRight: 25
  }
});

AppRegistry.registerComponent('takeabreak', () => takeabreak);
